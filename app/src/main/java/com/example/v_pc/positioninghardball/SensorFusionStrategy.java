package com.example.v_pc.positioninghardball;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.opengl.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import org.ejml.simple.SimpleMatrix;

public class SensorFusionStrategy implements PositioningStrategy {

    //inputs
    private SimpleMatrix z_n; // measurement
    private SimpleMatrix u_k; // control vector

    //Covariance Matrices
    private SimpleMatrix P; // covariance of prediction (error estimation)
    private SimpleMatrix R; // covariance of observation noise
    private SimpleMatrix Q; // covariance of process noise

    //Kalman
    private SimpleMatrix K; // kalman gain matrix

    //Constants
    private SimpleMatrix x_k; // system-state vector
    private SimpleMatrix F_k; // state-transition model
    private SimpleMatrix B_k; // control-input model
    private SimpleMatrix H; // observation model

    private Location initialLocation;
    private int dt = 1;

    private boolean vectorsInitialized = false;

    private LocationManager locationManager;
    private LocationListener locationListener;
    private MyMainActivity activity;
    private Context context;
    private SensorManager mSensorManager;
    private Sensor mSensorAccel;
    private Sensor mSensorRot;
    private Sensor mSensorGravity;
    private SensorEventListener accelListener;
    private SensorEventListener rotListener;
    private SensorEventListener graListener;
    private float[] accelValues;
    private float[] rotValues;
    private float[] graValues;
    private Handler handler;
    private Runnable runnable;
    private PositionData currentPos;

    public void setup(Activity activity) {
        this.activity = (MyMainActivity) activity;
        this.context = activity.getApplicationContext();
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensorAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorRot = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mSensorGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        Logger logger = new  Logger("SensorFusionStrategy");
        logger.write("Time, Lat, Long");

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Location loc = location;

                if (vectorsInitialized == false) {
                    if (initialLocation != null) {
                        initializeVectors(loc);
                        vectorsInitialized = true;
                    }
                    initialLocation = loc;

                }

                update_z_k(location);
                update_u_k();

                Log.d("SensorFusionStrategy", String.valueOf(location.getAccuracy()));
                Log.d("SensorFusionStrategy", location.toString());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        accelListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                accelValues = event.values;
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        rotListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                rotValues = event.values;
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        graListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                graValues = event.values;
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        mSensorManager.registerListener(accelListener, mSensorAccel, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(rotListener, mSensorRot, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(graListener, mSensorGravity, SensorManager.SENSOR_DELAY_GAME);

        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                if (vectorsInitialized) {
                    ((MyMainActivity) activity).clearTextField(3);
                    predict();
                    update();
                    currentPos = new PositionData(initialLocation.getLatitude() + x_k.get(1,0)*0.000009, initialLocation.getLongitude() + x_k.get(0,0) * 0.000016);
                    Log.d("SensorFusionStrategy", currentPos.toString());
                    ((MyMainActivity) activity).writeInTextField("SF: " + currentPos.getLatitude() + "," + currentPos.getLongitude(), 3);
                    logger.write(System.currentTimeMillis() + "," + currentPos.getLatitude() + "," + currentPos.getLongitude());

                }
                handler.postDelayed(runnable, (long) dt*1000);
            }
        }, (long) dt*1000);
    }

    @Override
    public void getLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("SensorFusionStrategy", "SensorStrategy couldn't be activated");
        }
        else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            Log.d("SensorFusionStrategy", "SensorStrategy activated");

        }
    }

    public float[] transformToWorldCoordinates() {
        float[] R = new float[9];
        float[] I = new float[9];
        SensorManager.getRotationMatrix(R, I, graValues, rotValues);
        float[] A_D = accelValues.clone();
        float[] A_W = new float[3];

        //Apply rotationMatrix
        A_W[0] = R[0] * A_D[0] + R[1] * A_D[1] + R[2] * A_D[2];
        A_W[1] = R[3] * A_D[0] + R[4] * A_D[1] + R[5] * A_D[2];
        A_W[2] = R[6] * A_D[0] + R[7] * A_D[1] + R[8] * A_D[2];

        return A_W;
    }

    public void initializeVectors(Location location) {
        initialize_x_k(location);
        initialize_F_k();
        initialize_B_k();
        initialize_R();
        initialize_H();
        initialize_Q();
        initialize_P();
    }

    public void initialize_x_k(Location location) {
        float[] A_W = transformToWorldCoordinates();
        x_k = new SimpleMatrix(4,1);

        x_k.set(0,0,0.0);
        x_k.set(1,0,0.0);
        x_k.set(2,0, location.distanceTo(initialLocation));
        x_k.set(3,0, location.distanceTo(initialLocation));
    }

    public void initialize_F_k() {
        double[][] init_F_k =
                        {{1.0, 0.0, dt, 0.0},
                        {0.0, 1.0, 0.0, dt},
                        {0.0, 0.0, 1.0, 0.0},
                        {0.0, 0.0, 0.0, 1.0}};
        F_k = new SimpleMatrix(init_F_k);
    }

    public void initialize_B_k() {
        double[][] init_B_k =
                        {{(Math.pow(dt,2)/2), 0.0},
                        {0.0, (Math.pow(dt,2)/2)},
                        {dt, 0.0},
                        {0.0, dt}};
        B_k = new SimpleMatrix(init_B_k);
    }

    public void update_u_k() {
        float[] A_W = transformToWorldCoordinates();
        if (u_k == null) {
            u_k = new SimpleMatrix(2,1);
            u_k.set(0,0, A_W[0]);
            u_k.set(1,0, A_W[1]);
        } else {
            u_k.set(0,0, A_W[0] - u_k.get(0,0));
            u_k.set(1,0, A_W[0] - u_k.get(1,0));
        }
    }

    public void update_z_k(Location loc) {
        float[] A_W = transformToWorldCoordinates();
        z_n = new SimpleMatrix(4,1);

        z_n.set(0,0,(loc.getLongitude() - initialLocation.getLongitude()) / 0.000016);
        z_n.set(1,0,(loc.getLatitude() - initialLocation.getLatitude()) / 0.000009);
        z_n.set(2,0, A_W[0]);
        z_n.set(3,0, A_W[1]);
    }

    public void initialize_H() {
        double[][] initH =
                        {{1.0, 0.0, 0.0, 0.0},
                        {0.0, 1.0, 0.0, 0.0},
                        {0.0, 0.0, 1.0, 0.0},
                        {0.0, 0.0, 0.0, 1.0}};
        H = new SimpleMatrix(initH);
    }

    public void initialize_R() {
        double[][] initR =
                        {{0.2, 0.0, 0.0, 0.0},
                        {0.0, 0.2, 0.0, 0.0},
                        {0.0, 0.0, 0.2, 0.0},
                        {0.0, 0.0, 0.0, 0.2}};
        R = new SimpleMatrix(initR);
    }

    public void initialize_Q() {
        double[][] initQ =
                        {{0.2, 0.0, 0.0, 0.0},
                        {0.0, 0.2, 0.0, 0.0},
                        {0.0, 0.0, 0.2, 0.0},
                        {0.0, 0.0, 0.0, 0.2}};
        Q = new SimpleMatrix(initQ);
    }

    public void initialize_P() {
        double[][] initP =
                        {{1000, 0.0, 0.0, 0.0},
                        {0.0, 1000, 0.0, 0.0},
                        {0.0, 0.0, 1000, 0.0},
                        {0.0, 0.0, 0.0, 1000}};
        P = new SimpleMatrix(initP);
    }

    public void predict() {
        x_k = F_k.mult(x_k).plus((B_k).mult(u_k));
        P = F_k.mult(P).mult(F_k.transpose()).plus(Q);
    }

    public void update() {
        K = P.mult(H.transpose()).mult(((H.mult(P).mult(H.transpose())).plus(R)).invert());

        x_k = x_k.plus(K.mult(z_n.minus(H.mult(x_k))));
        P = P.minus(K.mult(H).mult(P));

    }

    public PositionData getCurrentPos() {
        return currentPos;
    }

}
