package com.example.v_pc.positioninghardball;

class PositionData {

    private double latitude;
    private double longitude;
    private long time = System.currentTimeMillis();

    public PositionData(double lati, double longi) {
        latitude = lati;
        longitude = longi;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTime() { return time; }
}
