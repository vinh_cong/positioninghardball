package com.example.v_pc.positioninghardball;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Logger {

    private String name;
    File file;
    FileWriter fos;
    BufferedWriter bw;

    public Logger(String name) {
        this.name = name;

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);

        try {
            file = new File(Environment.getExternalStorageDirectory(), name + formattedDate + ".csv");

        } catch (Exception e) {
            Log.d("Exception ", e.toString());
        }
    }

    public boolean isExternalStorageWriteable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void writeNoNewLine(String s) {
        if (isExternalStorageWriteable()) {
            try {
                Log.d("Logger", "Wrote some Stuff");
                fos = new FileWriter(file, true);
                bw = new BufferedWriter(fos);
                bw.append(s);
                bw.flush();
                bw.close();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void write(String s) {
        if (isExternalStorageWriteable()) {
            try {
                Log.d("Logger", "Wrote some Stuff");
                fos = new FileWriter(file, true);
                bw = new BufferedWriter(fos);
                bw.append(s);
                bw.newLine();
                bw.flush();
                bw.close();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


}
