package com.example.v_pc.positioninghardball;

public class RSSIScan {
    private String ssid;
    private int rssi;

    public RSSIScan(String ssid, int rssi) {
        this.ssid = ssid;
        this.rssi = rssi;
    }

    public int getRssi() {
        return rssi;
    }

    public String getSsid() {
        return ssid;
    }
}
