package com.example.v_pc.positioninghardball;

class SSIDPositionData {

    private String name;
    private double latitude;
    private double longitude;
    private double x;
    private double y;
    private int rssi1meter;
    private double n;

    public SSIDPositionData(String name, double lati, double longi, double x, double y, int rssi1meter, double n) {
        this.name = name;
        latitude = lati;
        longitude = longi;
        this.x = x;
        this.y = y;
        this.rssi1meter = rssi1meter;
        this.n = n;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRssi1meter() {
        return rssi1meter;
    }

    public void setRssi1meter(int rssi1meter) {
        this.rssi1meter = rssi1meter;
    }


    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

}

