package com.example.v_pc.positioninghardball;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class WifiStrategy implements PositioningStrategy {
    private MyMainActivity activity;
    private Context context;
    private WifiManager wifiManager;
    private BroadcastReceiver wifiScanReceiver;
    private Handler handler;
    private int delay = 1000;
    private Runnable runnable;
    private List<SSIDPositionData> references;
    private List<ScanResultData> results;
    private PositionData currentPos;

    // Lat = NS. Long = EW

    //ref point 1 (Origin): 56.16612636, 10.12272771    - Cartesian System = (0,0)
    //ref point 2:          56.16610393, 10.12302461    - Cartesian System = (18.5,-2.5)
    //ref point 3:          56.16623227, 10.12288931    - Cartesian System = (10.1,11.8)

    //1 m in NS = 0.000009
    //1 m in EW = 0.000016


    @Override
    public void setup(final Activity activity) {
        this.activity = (MyMainActivity) activity;
        this.context = activity.getApplicationContext();
        wifiManager = (WifiManager) this.context.getSystemService(Context.WIFI_SERVICE);
        Logger logger = new  Logger("WifiStrategy");
        //logger.write("Time, Lat, Long");

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

        references = new ArrayList<SSIDPositionData>();
        references.add(new SSIDPositionData("AHBB", 56.16612636, 10.12272771,0, 0,-70,3.6));
        references.add(new SSIDPositionData("DIR510L-3BFF-5GHz", 56.16610393, 10.12302461, 18.5, -2.5, -45, 3.0));
        references.add(new SSIDPositionData("TP-LINK_7470AA", 56.16623227, 10.12288931, 10.1, 11.8, -41, 3.4));
        //references.add(new SSIDPositionData("ReferencePoint3", 56.16623227, 10.12288931, 10.1, 11.8, -41, 3.4));
        //references.add(new SSIDPositionData("Muhahahahaha", 56.16612636, 10.12272771, 10.1, 11.8, -41, 3.4));
        //references.add(new SSIDPositionData("dlink", 56.16612636, 10.12272771,0, 0,-70,3.6));

        wifiScanReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                results = new ArrayList<ScanResultData>();
                ((MyMainActivity) activity).clearTextField(1);
                List<ScanResult> wifiScanList = wifiManager.getScanResults();
                for(int i = 0; i < wifiScanList.size(); i++){
                    for (int j = 0; j < references.size(); j++) {
                        if (references.get(j).getName().equals(((wifiScanList.get(i).SSID)))) {
                            //logger.writeNoNewLine(wifiScanList.get(i).SSID + "," + wifiScanList.get(i).level + ",");

                            float distance = calculateDistance((wifiScanList.get(i).level), wifiScanList.get(i).SSID);
                            Log.d("WifiStrategy", distance + "m to " + wifiScanList.get(i).SSID);
                            results.add(new ScanResultData(references.get(j), distance));

                        }
                    }
                    Log.d("WifiStrategy", ((wifiScanList.get(i).SSID)) + "\t" + ((wifiScanList.get(i).level)));
                    //((MyMainActivity) activity).writeInTextField(((wifiScanList.get(i).SSID)) + "\t" + ((wifiScanList.get(i).level)) + "\n", 1 );
                }
                //logger.write("");


                currentPos = getLocation(results);
                if (currentPos != null) {
                    ((MyMainActivity) activity).writeInTextField((currentPos.getLatitude() + "," + currentPos.getLongitude()), 1);
                    logger.write(System.currentTimeMillis() + "," + currentPos.getLatitude() + "," + currentPos.getLongitude());
                }

            }
        };
        activity.registerReceiver(wifiScanReceiver, intentFilter);
    }

    public float calculateDistance(float rssi, String current) {
        SSIDPositionData currentRef = new SSIDPositionData("", 0, 0, 0, 0, 0, 0);
        for (int i = 0; i < references.size(); i++) {
            if (references.get(i).getName().equals(current)) {
                currentRef = references.get(i);
            }
        }
        double n = currentRef.getN();
        float distance = 0;
        int rssi1meter = currentRef.getRssi1meter();

        distance = (float) Math.pow(10, (rssi1meter - rssi) / (10 * n));
        return distance;
    };

    public PositionData getLocation(List<ScanResultData> results) {
        PositionData position;
        if (results.size() == 3) {

            ///Initialize variables for easier handling
            double da = results.get(0).getDistance();
            double db = results.get(1).getDistance();
            double dc = results.get(2).getDistance();

            double xa = results.get(0).getSsidpd().getX();
            double xb = results.get(1).getSsidpd().getX();
            double xc = results.get(2).getSsidpd().getX();

            double ya = results.get(0).getSsidpd().getY();
            double yb = results.get(1).getSsidpd().getY();
            double yc = results.get(2).getSsidpd().getY();

            //Do Trilateration
            double va = ((Math.pow(db, 2) - Math.pow(dc,2)) - (Math.pow(xb,2) - Math.pow(xc,2)) - (Math.pow(yb,2) - Math.pow(yc,2))) / 2;
            double vb = ((Math.pow(db, 2) - Math.pow(da,2)) - (Math.pow(xb,2) - Math.pow(xa,2)) - (Math.pow(yb,2) - Math.pow(ya,2))) / 2;
            double y = (vb * (xc - xb) - va * (xa - xb)) / ((ya - yb) * (xc - xb) - (yc - yb) * (xa - xb));
            double x = (va - y * (yc - yb)) / (xc - xb);
            Log.d("WifiStrategy", "x = " + x*0.000016 + ", and y = " + y*0.000009);

            //Reconvert to LatLong
            double lat = 56.166094 + (y * 0.000009);
            double lon = 10.122856 + (x * 0.000016);
            position = new PositionData(lat,lon);

            Log.d("WifiStrategy", position.getLatitude() + "," + position.getLongitude());

            return position;
        } else {
            return null;
        }
    }

    @Override
    public void getLocationUpdates() {
        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                wifiManager.startScan();
                handler.postDelayed(runnable, 1);
            }
        }, 1);
    };

    public PositionData getCurrentPos() {
        return currentPos;
    }
}
