package com.example.v_pc.positioninghardball;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class FingerPrintDB {
    private List<RSSIScan>[][] scans = new List[14][17];
    private List<String> references;
    String ref1 = "AHBB";
    String ref2 = "DIR510L-3BFF-5GHz";
    String ref3 = "TP-LINK_7470AA";
    String ref4 = "dlink";
    String ref5 = "DIR510L-3BFF";

    private int x = 14;
    private int y = 17;

    public FingerPrintDB() {
        references = new ArrayList<String>();
        references.add(ref1);
        references.add(ref2);
        references.add(ref3);
        references.add(ref4);
        references.add(ref5);

        scans[0][0] = createList(new int[] {-83, -83, -31, -74, -68});
        scans[0][1] = createList(new int[] {-90, -87, -30, -32, -70});
        scans[0][2] = createList(new int[] {-88, -78, -30, -29, -74});
        scans[0][3] = createList(new int[] {-86, -87, -30, -29, -71});
        scans[0][4] = createList(new int[] {-100, -84, -28, -71, -70});
        scans[0][5] = createList(new int[] {-81, -89, -20, -20, -71});
        scans[0][6] = createList(new int[] {-94, -84, -28, -28, -73});
        scans[0][7] = createList(new int[] {-27, -85, -26, -74, -75});
        scans[0][8] = createList(new int[] {-23, -84, -23, -76, -73});
        scans[0][9] = createList(new int[] {-23, -87, -23, -23, -69});
        scans[0][10] = createList(new int[] {-35, -85, -20, -27, -72});
        scans[0][11] = createList(new int[] {-80, -90, -23, -30, -74});
        scans[0][12] = createList(new int[] {-95, -89, -71, -70, -75});

        scans[1][0] = createList(new int[] {-81, -80, -28, -21, -71});
        scans[1][1] = createList(new int[] {-82, -83, -25, -27, -33});
        scans[1][2] = createList(new int[] {-104, -84, -30, -78, -73});
        scans[1][3] = createList(new int[] {-90, -85, -28, -70, -70});
        scans[1][4] = createList(new int[] {-98, -86, -24, -77, -32});
        scans[1][5] = createList(new int[] {-95, -86, -29, -73, -28});
        scans[1][6] = createList(new int[] {-88, -89, -28, -72, -72});
        scans[1][7] = createList(new int[] {-86, -85, -27, -70, -71});
        scans[1][8] = createList(new int[] {-90, -85, -29, -74, -73});
        scans[1][9] = createList(new int[] {-87, -85, -29, -71, -72});
        scans[1][11] = createList(new int[] {-91, -85, -74, -21, -74});
        scans[1][12] = createList(new int[] {-98, -90, -72, -70, -72});
        scans[1][13] = createList(new int[] {-80, -90, -71, -70, -72});
        scans[1][14] = createList(new int[] {-94, -88, -67, -71, -75});
        scans[1][15] = createList(new int[] {-80, -91, -69, -72, -77});

        scans[2][1] = createList(new int[] {-86, -79, -29, -75, -70});
        scans[2][2] = createList(new int[] {-105, -83, -28, -21, -35});
        scans[2][3] = createList(new int[] {-86, -81, -30, -70, -31});
        scans[2][4] = createList(new int[] {-88, -85, -28, -76, -32});
        scans[2][5] = createList(new int[] {-79, -86, -28, -74, -33});
        scans[2][6] = createList(new int[] {-77, -84, -28, -23, -70});
        scans[2][7] = createList(new int[] {-71, -82, -20, -22, -32});
        scans[2][8] = createList(new int[] {-100, -79, -23, -23, -27});
        scans[2][9] = createList(new int[] {-94, -83, -22, -20, -72});
        scans[2][11] = createList(new int[] {-79, -88, -66, -70, -73});
        scans[2][12] = createList(new int[] {-104, -87, -72, -70, -74});
        scans[2][13] = createList(new int[] {-93, -88, -72, -30, -73});
        scans[2][14] = createList(new int[] {-94, -88, -63, -33, -77});
        scans[2][15] = createList(new int[] {-80, -86, -67, -73, -73});

        scans[3][1] = createList(new int[] {-92, -81, -30, -75, -32});
        scans[3][2] = createList(new int[] {-102, -81, -29, -21, -28});
        scans[3][3] = createList(new int[] {-102, -80, -28, -71, -72});
        scans[3][4] = createList(new int[] {-83, -81, -30, -27, -74});
        scans[3][5] = createList(new int[] {-80, -83, -27, -70, -70});
        scans[3][6] = createList(new int[] {-78, -86, -26, -26, -71});
        scans[3][7] = createList(new int[] {-72, -80, -29, -22, -72});
        scans[3][8] = createList(new int[] {-93, -83, -26, -71, -72});
        scans[3][9] = createList(new int[] {-86, -80, -27, -70, -71});
        scans[3][11] = createList(new int[] {-85, -84, -72, -28, -81});
        scans[3][12] = createList(new int[] {-83, -87, -69, -27, -71});
        scans[3][13] = createList(new int[] {-81, -87, -72, -29, -75});
        scans[3][14] = createList(new int[] {-92, -84, -68, -28, -78});

        scans[4][3] = createList(new int[] {-89, -62, -24, -70, -23});
        scans[4][4] = createList(new int[] {-90, -58, -77, -20, -72});
        scans[4][5] = createList(new int[] {-86, -67, -71, -22, -27});
        scans[4][6] = createList(new int[] {-89, -72, -20, -71, -26});
        scans[4][7] = createList(new int[] {-84, -76, -20, -24, -34});
        scans[4][8] = createList(new int[] {-82, -82, -24, -27, -31});
        scans[4][9] = createList(new int[] {-80, -82, -70, -27, -76});
        scans[4][10] = createList(new int[] {-100, -86, -67, -22, -72});
        scans[4][11] = createList(new int[] {-91, -82, -72, -28, -72});
        scans[4][12] = createList(new int[] {-84, -85, -59, -70, -79});
        scans[4][13] = createList(new int[] {-84, -85, -60, -72, -71});
        scans[4][14] = createList(new int[] {-86, -80, -65, -72, -73});

        scans[5][3] = createList(new int[] {-88, -70, -27, -18, -23});
        scans[5][4] = createList(new int[] {-88, -66, -24, -23, -27});
        scans[5][5] = createList(new int[] {-100, -78, -22, -24, -30});
        scans[5][6] = createList(new int[] {-86, -79, -73, -21, -27});
        scans[5][7] = createList(new int[] {-85, -82, -63, -22, -70});
        scans[5][8] = createList(new int[] {-80, -83, -69, -28, -71});
        scans[5][9] = createList(new int[] {-105, -83, -66, -31, -80});
        scans[5][10] = createList(new int[] {-75, -80, -62, -75, -73});
        scans[5][11] = createList(new int[] {-101, -80, -66, -28, -73});
        scans[5][12] = createList(new int[] {-88, -78, -65, -31, -70});
        scans[5][13] = createList(new int[] {-84, -80, -58, -30, -70});
        scans[5][14] = createList(new int[] {-86, -80, -63, -31, -72});

        scans[6][5] = createList(new int[] {-87, -67, -28, -20, -25});
        scans[6][6] = createList(new int[] {-84, -70, -30, -76, -27});
        scans[6][7] = createList(new int[] {-87, -67, -23, -30, -28});
        scans[6][8] = createList(new int[] {-86, -79, -71, -26, -72});
        scans[6][9] = createList(new int[] {-83, -81, -72, -28, -71});
        scans[6][10] = createList(new int[] {-79, -77, -70, -30, -72});
        scans[6][11] = createList(new int[] {-79, -71, -66, -28, -30});
        scans[6][12] = createList(new int[] {-84, -67, -61, -31, -31});
        scans[6][13] = createList(new int[] {-86, -78, -61, -72, -69});
        scans[6][14] = createList(new int[] {-85, -70, -19, -28, -73});

        scans[7][5] = createList(new int[] {-83, -71, -29, -28, -30});
        scans[7][6] = createList(new int[] {-88, -70, -27, -18, -23});
        scans[7][7] = createList(new int[] {-104, -81, -23, -27, -35});
        scans[7][8] = createList(new int[] {-82, -82, -72, -27, -72});
        scans[7][9] = createList(new int[] {-89, -78, -70, -25, -69});
        scans[7][10] = createList(new int[] {-82, -76, -70, -76, -33});
        scans[7][11] = createList(new int[] {-100, -78, -71, -24, -70});
        scans[7][12] = createList(new int[] {-80, -79, -70, -24, -35});
        scans[7][13] = createList(new int[] {-103, -81, -66, -26, -78});
        scans[7][14] = createList(new int[] {-90, -78, -58, -24, -75});
        scans[7][15] = createList(new int[] {-103, -81, -11, -24, -75});

        scans[8][5] = createList(new int[] {-84, -74, -58, -27, -71});
        scans[8][6] = createList(new int[] {-104, -80, -61, -27, -69});
        scans[8][7] = createList(new int[] {-105, -77, -71, -29, -70});
        scans[8][8] = createList(new int[] {-101, -71, -72, -24, -31});
        scans[8][9] = createList(new int[] {-84, -63, -71, -30, -70});
        scans[8][10] = createList(new int[] {-83, -61, -22, -26, -27});
        scans[8][11] = createList(new int[] {-105, -74, -72, -69, -71});
        scans[8][12] = createList(new int[] {-92, -72, -28, -31, -27});
        scans[8][13] = createList(new int[] {-89, -71, -27, -28, -30});

        scans[9][5] = createList(new int[] {-82, -80, -67, -26, -72});
        scans[9][6] = createList(new int[] {-81, -82, -68, -24, -70});
        scans[9][7] = createList(new int[] {-83, -82, -69, -25, -71});
        scans[9][8] = createList(new int[] {-84, -80, -23, -28, -33});
        scans[9][9] = createList(new int[] {-82, -77, -69, -25, -71});
        scans[9][10] = createList(new int[] {-86, -74, -69, -29, -31});
        scans[9][11] = createList(new int[] {-84, -77, -21, -31, -23});
        scans[9][12] = createList(new int[] {-90, -79, -22, -27, -27});
        scans[9][13] = createList(new int[] {-85, -77, -25, -21, -29});

        scans[10][5] = createList(new int[] {-78, -80, -65, -24, -70});
        scans[10][6] = createList(new int[] {-82, -81, -65, -26, -70});
        scans[10][7] = createList(new int[] {-86, -84, -68, -70, -76});
        scans[10][8] = createList(new int[] {-87, -80, -20, -27, -70});
        scans[10][9] = createList(new int[] {-94, -69, -23, -26, -71});
        scans[10][10] = createList(new int[] {-84, -70, -24, -30, -30});
        scans[10][11] = createList(new int[] {-87, -64, -22, -33, -27});
        scans[10][12] = createList(new int[] {-87, -68, -23, -68, -28});
        scans[10][13] = createList(new int[] {-84, -58, -27, -29, -22});
        scans[10][14] = createList(new int[] {-86, -70, -25, -25, -23});

        scans[11][5] = createList(new int[] {-85, -62, -25, -26, -24});
        scans[11][6] = createList(new int[] {-85, -65, -29, -27, -23});
        scans[11][7] = createList(new int[] {-86, -69, -31, -70, -28});
        scans[11][8] = createList(new int[] {-88, -78, -22, -31, -33});
        scans[11][9] = createList(new int[] {-87, -80, -22, -29, -34});
        scans[11][10] = createList(new int[] {-88, -81, -23, -34, -32});
        scans[11][11] = createList(new int[] {-83, -79, -68, -30, -34});
        scans[11][12] = createList(new int[] {-82, -79, -63, -31, -35});

        scans[12][5] = createList(new int[] {-84, -68, -28, -31, -23});
        scans[12][6] = createList(new int[] {-88, -69, -25, -30, -27});
        scans[12][7] = createList(new int[] {-92, -77, -27, -71, -27});
        scans[12][8] = createList(new int[] {-86, -75, -22, -31, -28});
        scans[12][9] = createList(new int[] {-85, -74, -23, -28, -30});
        scans[12][10] = createList(new int[] {-88, -76, -76, -72, -29});

    }

    public List<RSSIScan> createList(int[] ints) {
        List<RSSIScan> result = new ArrayList<RSSIScan>();

        for (int i = 0; i < ints.length; i++) {
            result.add(new RSSIScan(references.get(i), ints[i]));
        }
        return result;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public List<String> getReferences() {
        return references;
    }

    public List<RSSIScan>[][] getScans() {
        return scans;
    }
}
