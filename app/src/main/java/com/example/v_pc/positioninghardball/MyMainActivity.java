package com.example.v_pc.positioninghardball;

import android.annotation.SuppressLint;
import android.location.Location;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MyMainActivity extends AppCompatActivity {

    private TextView myTextView;
    private PositioningStrategy strategy;
    private List<SSIDPositionData> references;
    private boolean requestingWifiScans = false;
    private Logger logger;
    private GPSStrategy gpsStrat;
    private WifiStrategy wifiStrat;
    private FingerPrintStrategy fpStrat;
    private SensorFusionStrategy fusionStrat;


    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        logger = new Logger("MainActivity");
        logAll();

        final ToggleButton toggleGPS = (ToggleButton) findViewById(R.id.toggleGPS);
        toggleGPS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gpsStrat = new GPSStrategy();
                    gpsStrat.setup(MyMainActivity.this);
                    gpsStrat.getLocationUpdates();
                    toggleGPS.setClickable(false);
                } else {
                    // The toggle is disabled
                }
            }
        });


        final ToggleButton toggleWifi = (ToggleButton) findViewById(R.id.toggleWIFI);
        toggleWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    wifiStrat = new WifiStrategy();
                    wifiStrat.setup(MyMainActivity.this);
                    if (!requestingWifiScans) {
                        requestingWifiScans = true;
                        wifiStrat.getLocationUpdates();
                    }
                    toggleWifi.setClickable(false);
                } else {
                    // The toggle is disabled
                }
            }
        });

        final ToggleButton toggleFP = (ToggleButton) findViewById(R.id.toggleFP);
        toggleFP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    fpStrat = new FingerPrintStrategy();
                    fpStrat.setup(MyMainActivity.this);
                    if (!requestingWifiScans) {
                        requestingWifiScans = true;
                        fpStrat.getLocationUpdates();
                    }
                    toggleFP.setClickable(false);
                } else {
                    // The toggle is disabled
                }
            }
        });

        final ToggleButton toggleFusion = (ToggleButton) findViewById(R.id.toggleFUSION);
        toggleFusion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    fusionStrat = new SensorFusionStrategy();
                    fusionStrat.setup(MyMainActivity.this);
                    fusionStrat.getLocationUpdates();
                    toggleFusion.setClickable(false);
                } else {
                    // The toggle is disabled
                }
            }
        });

        final Button button = findViewById(R.id.buttonTime);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                logger.write(System.currentTimeMillis() + "");
            }
        });
    }

    public void writeInTextField(String s, int id) {
        if (id == 0) {
            myTextView = findViewById(R.id.textViewGPS);
        } else if (id == 1) {
            myTextView = findViewById(R.id.textViewWifi);
        } else if (id == 2) {
            myTextView = findViewById(R.id.textViewFP);
        } else if (id == 3) {
            myTextView = findViewById(R.id.textViewFusion);
        }

        myTextView.append(s);
    }

    public void clearTextField(int id) {
        if (id == 0) {
            myTextView = findViewById(R.id.textViewGPS);
        } else if (id == 1) {
            myTextView = findViewById(R.id.textViewWifi);
        } else if (id == 2) {
            myTextView = findViewById(R.id.textViewFP);
        } else if (id == 3) {
            myTextView = findViewById(R.id.textViewFusion);
        }
        myTextView.setText("");
    }

    public void logAll() {
        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                logger.writeNoNewLine(System.currentTimeMillis() + ",");
                if (gpsStrat != null && gpsStrat.getCurrentPos() != null) {
                    logger.writeNoNewLine("GPSStrat," + gpsStrat.getCurrentPos().getLatitude() + "," + gpsStrat.getCurrentPos().getLongitude() + "," + gpsStrat.getCurrentPos().getTime() + ",");
                }
                if (wifiStrat != null && wifiStrat.getCurrentPos() != null) {
                    logger.writeNoNewLine("WifiStrat," + wifiStrat.getCurrentPos().getLatitude() + "," + wifiStrat.getCurrentPos().getLongitude() + "," + wifiStrat.getCurrentPos().getTime() + ",");
                }
                if (fpStrat != null && fpStrat.getCurrentPos() != null) {
                    logger.writeNoNewLine("FPStrat," + fpStrat.getCurrentPos().getLatitude() + "," + fpStrat.getCurrentPos().getLongitude() + "," + fpStrat.getCurrentPos().getTime() + ",");
                }
                if (fusionStrat != null && fusionStrat.getCurrentPos() != null) {
                    logger.writeNoNewLine("FusionStrat," + fusionStrat.getCurrentPos().getLatitude() + "," + fusionStrat.getCurrentPos().getLongitude() + "," + fusionStrat.getCurrentPos().getTime() + ",");
                }

                logger.write("");
                handler.postDelayed(runnable, 1000);
            }
        }, 1000);
    }

}
