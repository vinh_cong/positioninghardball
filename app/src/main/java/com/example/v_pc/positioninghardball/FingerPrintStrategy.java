package com.example.v_pc.positioninghardball;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class FingerPrintStrategy implements PositioningStrategy {
    private Activity activity;
    private Context context;
    private BroadcastReceiver wifiScanReceiver;
    private Handler handler;
    private int delay = 1000;
    private Runnable runnable;
    private Logger logger;
    private FingerPrintDB fpdb = new FingerPrintDB();
    private PositionData currentPos;
    private int[] res = new int[2];
    private SensorManager mSensorManager;
    private Sensor mSensorAccel;
    private float[] accelValues;
    private SensorEventListener accelListener;
    private WifiManager wifiManager;


    //ref point 1 (Origin): 56.16612636, 10.12272771    - Cartesian System = (0,0)
    //Our grid's (0,0) is located at (2, -5) in the system with refpoint 1 as origin

    private double originLat = 56.16612636;
    private double originLong = 10.12272771;

    //1 m in NS = 0.000009
    //1 m in EW = 0.000016

    @Override
    public void setup(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        wifiManager = (WifiManager) this.context.getSystemService(Context.WIFI_SERVICE);
        //mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        //mSensorAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        logger = new Logger("FingerPrintStrategy");

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

        /*
        accelListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                accelValues = event.values;
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        */

        wifiScanReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ((MyMainActivity) activity).clearTextField(2);
                List<ScanResult> wifiScanList = wifiManager.getScanResults();
                ArrayList<RSSIScan> results = new ArrayList<>();
                for(int i = 0; i < wifiScanList.size(); i++){
                    for(int j = 0; j < fpdb.getReferences().size(); j++) {
                        if (wifiScanList.get(i).SSID.equals(fpdb.getReferences().get(j))) {
                            results.add(new RSSIScan(wifiScanList.get(i).SSID, wifiScanList.get(i).level));
                        }
                    }
                }
                if (currentPos == null) {
                    Log.d("FingerPrintStrategy", "Position is null");
                    currentPos = getFPPosition(null, results);
                } else {
                    Log.d("FingerPrintStrategy", "Position not null");
                    currentPos = getFPPosition(currentPos, results);
                    Log.d("FingerPrintStrategy", "New Pos: "  + (currentPos.getLongitude()-originLong) / 0.000016 + "," + (currentPos.getLatitude()-originLat) / 0.000009);
                }
                if (currentPos != null) {
                    logger.write(System.currentTimeMillis() + "," + currentPos.getLatitude() + "," +currentPos.getLongitude());
                    ((MyMainActivity) activity).writeInTextField(currentPos.getLatitude() + ","  + currentPos.getLongitude(),2);
                }
            }
        };
        activity.registerReceiver(wifiScanReceiver, intentFilter);
        //mSensorManager.registerListener(accelListener, mSensorAccel, SensorManager.SENSOR_DELAY_GAME);
    }

    public PositionData getFPPosition(PositionData current, ArrayList<RSSIScan> currentScans) {
        //int movement = (int) Math.ceil(Math.sqrt(( accelValues[0] + accelValues[1] + accelValues[2])));
        int movement = 2;
        Log.d("FingerPrintStrategy", "movement:" + movement);
        double minValue;

        //Case: No current Position
        if (current == null) {
            minValue = Double.POSITIVE_INFINITY;

            for (int x=0; x < fpdb.getX(); x++) {
                for (int y=0; y< fpdb.getY(); y++) {
                    if (fpdb.getScans()[x][y] == null) {
                        Log.d("FingerPrintStrategy", "Found null array");
                        continue;
                    } else {
                        double NNvalue = 0.0;
                        for (int i=0; i<currentScans.size(); i++) {
                            for (int j=0; j<fpdb.getReferences().size(); j++) {
                                if (currentScans.get(i).getSsid().equals(fpdb.getReferences().get(j))) {
                                    NNvalue += Math.pow(currentScans.get(i).getRssi() - fpdb.getScans()[x][y].get(j).getRssi(), 2);
                                }
                            }
                        }
                        NNvalue = Math.sqrt(NNvalue);

                        if (NNvalue < minValue) {
                            minValue = NNvalue;
                            res[0] = x;
                            res[1] = y;
                        }
                    }
                }
            }

        // Case: have current position
        } else {
            minValue = Double.POSITIVE_INFINITY;

            int currentX = res[0];
            int currentY = res[1];

            int searchXmin = Math.max(0, currentX - movement);
            int searchYmin = Math.max(0, currentY - movement);

            int searchXmax = Math.min(fpdb.getX(), currentX + movement);
            int searchYmax = Math.min(fpdb.getY(), currentY + movement);

            Log.d("FingerPrintStrategy", "Current Pos in new coodsys: " + currentX + "," + currentY);
            Log.d("FingerPrintStrategy", "Searching:" + searchXmin + "," + searchYmin + "to" + searchXmax + "," + searchYmax);

            for (int x = searchXmin; x < searchXmax; x++) {
                for (int y = searchYmin; y < searchYmax; y++) {
                    if (fpdb.getScans()[x][y] == null) {
                        Log.d("FingerPrintStrategy", "Found null array");
                        continue;
                    } else {
                        double NNvalue = 0.0;
                        for (int i=0; i<currentScans.size(); i++) {
                            for (int j=0; j<fpdb.getReferences().size(); j++) {
                                if (currentScans.get(i).getSsid().equals(fpdb.getReferences().get(j))) {
                                    NNvalue += Math.pow(currentScans.get(i).getRssi() - fpdb.getScans()[x][y].get(j).getRssi(), 2);
                                }
                            }
                        }
                        NNvalue = Math.sqrt(NNvalue);
                        Log.d("FingerPrintStrategy", "NNValue:" + NNvalue);

                        if (NNvalue < minValue) {
                            minValue = NNvalue;
                            res[0] = x;
                            res[1] = y;
                        }
                    }
                }
            }
        }
        Log.d("FingerPrintStrategy", res[0] + "," + res[1]);
        ((MyMainActivity) activity).writeInTextField(res[0] + "," + res[1] + "\n",2);
        return new PositionData(originLat + (-5+res[1])*0.000009, originLong + (2+res[0])*0.000016);
    }

    @Override
    public void getLocationUpdates() {
        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                wifiManager.startScan();
                handler.postDelayed(runnable, 1);
            }
        }, 1);
    };

    public PositionData getCurrentPos() {
        return currentPos;
    }
}
