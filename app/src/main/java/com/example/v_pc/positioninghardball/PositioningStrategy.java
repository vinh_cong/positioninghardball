package com.example.v_pc.positioninghardball;

import android.app.Activity;
import android.location.Location;

public interface PositioningStrategy {

    void setup(Activity activity);

    void getLocationUpdates();

    PositionData getCurrentPos();
}
