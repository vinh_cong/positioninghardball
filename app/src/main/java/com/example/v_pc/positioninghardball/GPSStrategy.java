package com.example.v_pc.positioninghardball;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class GPSStrategy implements PositioningStrategy {

    private LocationManager locationManager;
    private LocationListener locationListener;
    private MyMainActivity activity;
    private Context context;
    private PositionData currentPos;

    @Override
    public void setup(Activity activity) {
        this.activity = (MyMainActivity) activity;
        this.context = activity.getApplicationContext();
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Logger logger = new  Logger("GPSStrategy");
        logger.write("Time, Lat, Long, Acc");


        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                ((MyMainActivity) activity).clearTextField(0);
                Log.d("GPSStrategy", String.valueOf(location.getAccuracy()));
                Log.d("GPSStrategy", location.toString());
                ((MyMainActivity) activity).writeInTextField("GPS: " + location.getLatitude() + "," + location.getLongitude() + ", acc = " + location.getAccuracy(), 0);
                logger.write(System.currentTimeMillis() + "," + location.getLatitude() + "," + location.getLongitude() + ", " + location.getAccuracy());
                currentPos = new PositionData(location.getLatitude(), location.getLongitude());

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };



    }

    @Override
    public void getLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("GPSStrategy", "GPSStrategy couldn't be activated");
        }
        else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            Log.d("GPSStrategy", "GPSStrategy activated");

        }
    }

    public PositionData getCurrentPos() {
        return currentPos;
    }

}
