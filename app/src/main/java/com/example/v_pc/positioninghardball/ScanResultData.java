package com.example.v_pc.positioninghardball;

public class ScanResultData {

    private SSIDPositionData ssidpd;
    private float distance;

    public ScanResultData(SSIDPositionData ssidpd, float distance) {
        this.ssidpd = ssidpd;
        this.distance = distance;
    }

    public float getDistance() {
        return distance;
    }

    public SSIDPositionData getSsidpd() {
        return ssidpd;
    }
}
