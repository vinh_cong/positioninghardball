\chapter{Design and Methodology}
\label{cha:design-and-method}

In the previous sections, we defined several hypotheses that we wish to test. Dimensions has been defined to better grasp the elements which we deem important for the design. Finally, base requirements has been constructed to specify minimum capabilities of the envisioned system. Based on these factors, this section describes our vision on the system, how it is constructed and how it should function. The section is separated into subsections describing the technology chosen, positioning techniques utilized, the infrastructure of the system, and the evaluations planned for testing the system.

\section{Technology}
\label{sec:des-techo}

We have chosen our main positioning technology to be RF-based due to the several reasons. 
Firstly, the fact that systems employing this technology have a broad range of coverage is highly preferred. As previously mentioned in \autoref{cha:analysis}. Since the field spans over 5000 m\textsuperscript{2}, choosing RF-based technology will help us cover the whole field.
Secondly, we can argue that sub meter levels of accuracy is unnecessary for our specific scenario, since a player can easily move up to several meters in a single second, providing information on players' approximate location with inaccuracies of a few meters should be sufficient. Of course it would be preferable to achieve accuracy of sub meter range for analysis of team movement and strategy planning, but achieving that would be a tradeoff between cost and accuracy.
Thirdly, since there are no tall buildings or other obstructions in the near vicinity of the playing field, we can avoid most issues with reflections and multipaths, increasing the overall accuracy of the system with no added cost.

Since an RF-based technology is available and easily accessible, namely GPS technology, in the smart phone, we will utilize this device as the central component for positioning within the field. However, this requires extra caution in the field, as a bullet fired from the airsoft weapons can easily break the glass screens of the smart phones.
An added bonus of choosing the smart phone as the central positioning tool is the availability of other sensors, that can be used in conjunction with GPS technology. We will have access to wi-fi receivers, which in turn enables techniques such as wi-fi positioning and fingerprinting. Thus we shall incorporate those techniques to help increase the accuracy of the system.
Inertial sensors are also available for use. Hence we will incorporate sensor fusion for positioning as well. 

The following section depicts an in-depth explanation of the design of each technique utilized.

\section{Positioning Techniques}
\label{sec:des-techni}

In this section, an in-depth description of the positioning techniques of the system is provided. The system design for each technique is presented separately. The techniques presented includes:

\begin{itemize}
	\item{GPS Services}
	\item{Wi-fi Location and Fingerprinting}
	\item{Sensor Fusion}
\end{itemize}

Since we are using Android as our platform, we intend to program in Android Studio, which results in programming in Java language. This also means that a number of base functionality is already implemented in the Android API.

\subsection{GPS Services}

Base GPS Services are relatively straight forward to implement using Google's Location Services API. Configuring the settings is the more complicated issue. The Location Services API provides access to several different positioning systems. The coarse Cell-ID and Wi-fi signal based positioning for indoor and outdoor use and the finer GPS positioning for only outdoor use, albeit heavier on battery usage. This will be our main model for GPS services, and since we are striving towards a system usable in real-time and and a system where our accuracy is as high as possible, this aligns well with the intended usage of the GPS services within the API. Using GPS in conjunction with the other available positioning systems does however decrease the time-to-first-fix (TTFF), which is the start-up time of the system. That is getting our position with no or unusable prior location information.

\subsection{Wi-fi Location}

For wi-fi and fingerprinting technologies, we roughly follow the approach described in \citep{quan2010:WLuRF} on RSSI fingerprinting. For wi-fi positioning, the necessary hardware includes a device capable of receiving wi-fi signals and 3 or more access points. Using trilateration, in our case specifically the geometry of circles, we can, by converting signal strength to distances, identify positions where the circles overlap and thus calculate the position of the device. There exists a number of techniques for calculating distances from RSS. The method we plan to employ is described in Oguejiofor et all's work with outdoor positioning systems using RSSI measurements \citep{oguejiofor2013:OLSuRMoWSN}.

Given 3 access points A, B, C with positions (xa, ya), (xb, yb), (xc, yc). And using the formula of a sphere:

\begin{equation}
		(d_a)^2 = (x-xa)^2 + (y-ya)^2
		\label{eq:1}
\end{equation}

The distance between the device and the access points are defined as $d_a$, $d_b$ and $d_c$. The formulas for deriving a position (x,y) in a plane given 3 access points is the following:

\begin{equation}
		y = \frac{v_b(xc-xb)-v_a(xa-xb)}{(ya-yb)(xc-xb)-(yc-yb)(xa-xb)}
		\label{eq:2}
\end{equation}
	
\begin{equation}
		x = \frac{v_a-y(yc-yb)}{(xc-xb}
		\label{eq:3}
\end{equation}
	
\begin{equation}
		v_a = \frac{(d_b^2-d_c^2)-(xb^2-xc^2)-(yb^2-yc^2)}{2}
		\label{eq:4}
\end{equation}
	
\begin{equation}
		v_b = \frac{(d_b^2-d_a^2)-(xb^2-xa^2)-(yb^2-ya^2)}{2}
		\label{eq:5}
\end{equation}

To be able to compute these elements, a signal propagation model is necessary to convert from RSSI to distance. Several factors influences the signal strength received at the device whose position we aim to compute, such as length traveled, obstructions, reflection, multipath etc. 
From empirical data, the following power law model predicts the loss while the signal travels:

\begin{equation}
		P_L(d_i)[dB] = P_L(d_0)[dB] + 10n log(\frac{d_i}{d_0}) + S
		\label{eq:6}
\end{equation}

Where the first product is the path loss at a known reference distance $d_0$.
n = path loss exponent.
S = shadowing factor as a Gaussian random variable with standard deviation $\sigma[db]$.

To evaluate the path loss exponent at a given area/environment, we can manually compute with the formula:

\begin{equation}
		n = \frac{\{P_L(d_i)-P_L(d_0)\}}{10log(\frac{d_i}{d_0})}
		\label{eq:7}
\end{equation}

RSSI is related to distance using the following equation:

\begin{equation}
		RSSI[dBM] = -10n \logten(d) + A[dBM]
		\label{eq:8}
\end{equation}
		
where d = distance.
A = received signal strength at one meter of distance.

To determine path loss exponent, we measure RSSI values at given intervals and take the RMS of the measured values for a optimal estimation.

Using these formulas, we can calculate a position given three or more access points.

Since we cannot cover the whole field of Aarhus Hardballbane using only 3 access points, the wifi positioning will only be available in a larger part of the field. The reason being that we need accurate positions of the access points to perform trilateration. We argue that if it functions well in a smaller scale, it will also function well when scaling size by adding more access points.

\subsection{RSSI Fingerprinting}

For RSSI Fingerprinting, we follow the general directions explained in \citep{quan2010:WLuRF}. 

For this technique, we split the specified quadrant of the field into zones consisting of $1m^2$. Then we measure the RSSI from available access points and save all the data to a database. Given each access point, we can then create a Fingerprint Map featuring the quadrant and RSSI values measured within each $1m^2$ in regards to every access point.

For positioning, we use the nearest neighbor approach explained within the article. This method utilizes Euclidean distances by calculating that distance between the live RSSI reading and each access point fingerprint. The minimum is then the nearest neighbor and likely the (x,y) location.

\begin{equation}
		NN = \min_{j = 0 ... k}(\sqrt{\sum_{i=0}^{N}{(R_i-FP_i)^2}})
\label{eq:9}
\end{equation}

where FP zones = j = 0 to k, FP in that zone = i = 0 to N.

We can constrain the algorithm with a maximal traveling distance, such that the prediction cannot jump distances that is deemed impossible for a person to move. We can also constrain the search based on sensor data from the accelerometer.

\subsection{Sensor Fusion}

For this technique, we follow the same approach as presented by Caron et all. \citep{Caron2003:Gdfumkfioca}. Our implementation of sensor fusion should be able provide better estimations of location by utilizing a Kalman filter to filter noise from meaningful data. Our model differs from the presented model in \citep{Caron2003:Gdfumkfioca} as their model aim to capture movement of a vehicle. We assume a simplified model as we do not have the necessary instruments, to measure information to ideally capture human motion, such as muscle movement. Our simplified model follows the guidelines described in \footnote{https://www.bzarg.com/p/how-a-kalman-filter-works-in-pictures/#mjx-eqn-kalupdatefull}. We shall assume zero mean white Gaussian noise. Let $\^x_{k}$ be our best estimate with its covariance matrix $P_{k}$.

\begin{equation}
	\^x_{k} = \begin{bmatrix}
							position \\
							velocity
						\end{bmatrix}
\label{eq:10}
\end{equation}

\begin{equation}
	P_{k} = \begin{bmatrix}
						\sum_{pp} & \sum_{pv} \\
						\sum_{vp} & \sum_{vv}
					\end{bmatrix}
\label{eq:11}
\end{equation}

We assume a basic kinetic prediction formula, where position is influenced by velocity.

\begin{equation}
	\^{x}_{k} = \begin{bmatrix}
								1 & \Delta{t} \\
								0 & 1
							\end{bmatrix} \^{x}_{k-1}
\label{eq:12}
\end{equation}

Where the prediction matrix is labeled $F_{k}$ such that:

\begin{equation}
	\^{x}_{k} = F_{k}\^{x}_{k-1}
\label{eq:13}
\end{equation}

Factoring external influences into the prediction. We add $B_{k}$ called a control matrix and $\overrightarrow{u_{k}}$ called the control vector:

\begin{equation}
	\^{x}_{k} = F_{k}\^{x}_{k-1} + B_{k}\overrightarrow{u_{k}}
\label{eq:14}
\end{equation}

To update covariance matrices $P_{k}$ that includes external uncertainty $Q_{k}$, we compute:

\begin{equation}
	P_{k}=F_{k}P_{k-1}F_{k}^{T} + Q_{k}
\label{eq:15}
\end{equation}

Let $H_{k}$ be a matrix modeling sensor readings. Let $R_{k}$ be the covariance of the uncertainty of the sensors. This distribution should have a mean equal to the reading observed. Let $\overrightarrow{z_{k}}$ be that mean. 

We now have two Gaussian distributions of respectively predicted and observed data. Let K be a matrix called the Kalman gain. Using the Kalman gain and matrices defining the distributions we can compute the update step of the Kalman filter:

\begin{equation}
	K' = P_{k}H_{k}^{T}(H_{k}P_{k}H_{k}^{T}+R_{k})^{-1}
\label{eq:16}
\end{equation}

\begin{equation}
	\begin{split}
		\^{x'}_{k} = \^{x}_{k}+K'(\overrightarrow{z_{k}}-H_{k}\^{x}_{k}) \\
		P'_{k} = P_{k}-K'H_{k}P_{k}
	\end{split}
\label{eq:17}
\end{equation}

Thereby updating the filter for the next round of predictions.

\section{Infrastructure}
\label{sec:des-inf}

The infrastructure of the system is centralized, where we will have a computer act as a receiver for data send from devices in the field. The devices themselves are responsible for using the chosen techniques to calculate their positions based on sensor feedback. Then they forward the computed positions to the central computer using the local area network. Communications will be based on standard LAN protocols. 
Additional routers will be installed to support LAN functionality across the whole game field as well as for providing reference points for wifi positioning and fingerprinting. The additional router will be wired directly to a main router using Ethernet cables and the network is configured such that it functions as a single network.

A simplified UML diagram sketching the system is shown in \autoref{fig:uml}

\begin{figure}
	\centering
		\includegraphics[width=\linewidth]{gfx/figure2}
	\caption{An UML diagram sketching the system.}
	\label{fig:uml}
\end{figure}

\newpage
\section{Evaluation}
\label{sec:des-eva}

Evaluation of the system will be separated into two different scenarios where we perform tests using combinations of the different techniques employed. The scenarios includes:

\textbf{A controlled field study}, where we construct a course through the field with checkpoints with known positions for better control over the study. Using the evaluation from Macleod et all.'s study \citep{Macleod2009:Tvoangpsfapmpifh} on hockey players' movement patterns as a template, we construct a similar experiment with a determined course through the field. Using the calculated positions as comparisons, we can easily spot inaccuracy using deviance from the constructed course.

\textbf{An uncontrolled field study}, where we provide players with a positioning device in a real game of hardball. In this case, we will test how well the system can perform under its intended use in the dimensions of reliability and latency. As real positions are unavailable to us, it will prove difficult to model precise accuracy. We can however measure the error rate between the proposed techniques and yest how much results vary from each other. Reliability will be measured using sensor feedback and fluctuation in behavior. Latency is measured by synchronizing clocks and comparing TOF of messages. This test will require the cooperation of the players. As such we will schedule evaluations on regular game days to achieve an as uncontrolled evaluation as possible.


  
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ClassicThesis"
%%% End:
