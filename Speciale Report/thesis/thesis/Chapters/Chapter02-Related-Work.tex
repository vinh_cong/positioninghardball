\chapter{Related Work}
\label{cha:related-work}

The problem of local positioning is a well explored area. Existing systems should be reviewed for comparison of strengths and weaknesses to guide design choices made for our implementation of a LPS.
For this purpose, we shall define several dimensions that we deem important. These dimensions serves as guidelines for analyzing related work.

In R. Mautz’s work \citep{Mautz2009:TCoIEaSosAPS2009} on challenges and specification of alternative positioning systems, he uses several dimensions to describe and compare technologies. Although he works with an indoor environment, we can in most cases translate the analysis to outdoor scenarios, as the indoors are usually more limiting in comparison, due to signal blockage of walls and furniture.
We shall use some of the dimensions presented in his work to guide our own analysis. Some of these are closely related to the hypothesis described in the previous section:

\textbf{Signal Type:} The signal type can heavily influence the strength of the system based on the local environment

\textbf{Indoor/Outdoor:} Since we are planning an outdoor system, we should adjust accordingly

\textbf{Accuracy:} Accuracy is a central aspect needed within the system and should be one of the main concerns.

\textbf{Range:} Range is another main aspect, as we would like to cover the entire field with the system.

\textbf{Hardware:} The hardware used should be realistically possible for us to attain and utilize for implementation of our system.

\textbf{Latency:} Whether the system can provide sufficiently low latency to cover real-time tracking of devices

There are two categories of systems in related work, that we have analyzed. These are systems designed with a goal of providing a system for tracking and positioning and systems designed in the context of games, where positioning is central. 

We shall present our analysis of related work in two subsections. The first subsection describes methods and technologies in positioning systems with regards to the dimensions defined. The second subsection describes similar aspects but in regards to positioning systems utilized for the purpose of games or similar.

\section{Positioning Systems}
\label{sec:pos-systems}

Numerous systems have been constructed to address the problem of positioning. As a general approach, we have chosen to only explore systems featuring real-time positioning as that feature is highly relevant for our system. For the systems explored, an aspect making then easily separable is the transmission type. We shall present findings separated by this.
Most of the systems to be described utilizes lateration as a technique for determining position. Lateration is the art of using measurements of distances in conjuction with geometric shapes like circles, squares, triangles etc. to calculate relative or absolute positions. 

\subsection{Radio Frequency}

RF-based systems are among the most utilized types of systems for positioning. The well known GPS is one of many systems based on radio signals. Today, RF-based systems can offer accuracy between the m and mm scale, highly depending on chosen technology and in cases environment.
RF-based systems offers the advantage of not requiring line of sight for the positioning to function. Signal blockage is however a factor which highly influences the degree of performance, as can be seen with indoor versus outdoor GPS tracking.
RF-based systems also suffer from reflections as it obscures the direct signal path with multiple other paths via reflections, resulting in inaccurate estimation of positioning. 

Locata \citep{Barnes2003:LANPTfHPIaOP} is a highly specialized RF-based system utilizing lateration, able to provide accuracy of sub-cm level. However, it requires a specialized installation of a non-affordable system. Locata provides coverage in ranges of 2 km, before systems becomes unreliable.

Geolocation and Assisted GPS \citep{Djuknic2001:GaAG} is a low cost solution utilizing GPS signals and assisted GPS stations to provide support for lateration, yielding results in the meters metric in the outdoor environment. On the other hand, \citep{Djuknic2001:GaAG} can be provided globally depending on whether A-GPS stations are present in the near environment. 

LandMarc \citep{Ni2004:LILSUAR} is another RF-based system utilizing long range RFID scanners and active RFID reference tags to perform positioning based on signal strengths. The RFID system is designed for indoor usage and can provide an accuracy of dm-m. The system can provide coverage of 20 m. With a relative low cost compared to Locata \citep{Barnes2003:LANPTfHPIaOP}. This solution provides an alternative for indoor positioning.

In Navarro et all's work \citep{quan2010:WLuRF} with RSSI (Received Signal Strength Indicator) Fingerprinting, they describe two positioning systems. One using wi-fi signals from multiple access points to calculate position. The technique used for the positioning itself is triangulation, that is using triangles for computation of position. The second system utilizes fingerprinting, a radio map of a given area is generated using RSSI data from multiple access points in range, which is used to generate a probabilistic distribution for use. A number of different techniques are used for predicting position, including nearest neighbors and Markov localization. They conclude that the first system using triangulation is insufficient for their use case, which is a outdoor play are for children. The fingerprinting approach yields however, yields better results of mean errors from 60 cm to 120 cm. Coverage is not mentioned, but given the nature of requiring multiple access points for fingerprinting, and a access points typically has a range of 30 m. The coverage will scale with additional access points.

A slightly different approach is taken in Oguejiofor et all's work \citep{oguejiofor2013:OLSuRMoWSN} on an outdoor positioning system using RSSI measurements. Here they employ trilateration with a wireless sensor network, where some nodes know of their own position. Using packets send between the devices, the devices whose positions are unknown can be computed using nodes whose position is known. Their results in levels of accuracy is in the sub-m category.

The fusion between GPS and inertial navigation systems can prove highly valuable for increasing system performance. In Caron et. all's work \citep{Caron2003:Gdfumkfioca} on this topic, they achieve a 60\% boost in accuracy of the system when compared to standalone GPS tracking. 

\subsection{Ultrasound}

Specialized for indoor positioning. Utilizing ultrasound to perform lateration provides these systems with accuracies in the cm range. They suffer from similar issues as RF-based systems with blockages and reflections of ultrasound waves between transmitters and receivers, resulting in less accuracy. Ultrasound systems requires specialized installations to function properly. 

Active Bat \citep{Hightower2001:LSfUC} is a such system requiring installations of transmitters in the ceiling for location services. The system can provide coverage of 1000 m\textsuperscript{2}, at a moderate cost compared to Locata \citep{Barnes2003:LANPTfHPIaOP} and LandMarc \citep{Ni2004:LILSUAR}.

Cricket \citep{Priyantha2000:TCLSS} can be employed using off-the-shelf hardware at costs of approximately 10 USD per transmitter/receiver. Offering the same level of accuracy as Active Bat \citep{Hightower2001:LSfUC}, it comes with the disadvantage of a range in the 10 m scale. 

\subsection{Infrared}

Using infrared technology comes with the disadvantage of requiring direct line of sight between a camera and the objects. With the camera's extrinsics and intrinsics, the relative positions of objects are calculated using lateration. This is done using reference objects installed in the cameras line of sight. These solutions offers accuracy at the mm level with limited coverage in the m scale. 

Firefly \footnote{\url{http://www.cybernet.com/interactive/firefly} accessed 2018/10/21.} is an installation typically used in medical or motion capture context. where extremely high levels of accuracy is needed. 

IR Smart Devices \citep{Aitenbichler2003:AILPSfSIaD} provides lower accuracy with cm-level errors but higher range of 100 m\textsuperscript{2} compared to Firefly. With off-the-shelf hardware of lower costs.

Positioning systems offers different strengths and weaknesses. Typically depending on level of investment, context and environment. \autoref{tab:possys-summary} summarizing our findings on related systems.

\begin{sidewaystable}[ph!]
		\myfloatalign
	\begin{tabularx}{\textwidth}{Xccccccc} \toprule
		\tableheadline{System} & \tableheadline{Type} & \tableheadline{Indoor} & \tableheadline{Outdoor} & \tableheadline{Accuracy} & \tableheadline{Coverage} & \tableheadline{Cost} & \tableheadline{Frequency} \\ \midrule
		Locata & RF & Y & Y & mm to cm & 2 km & High & 1 Hz \\
		A-GPS & RF  & (Y) & Y & m & Global & Low & 1 Hz \\
		LandMarc & RF  & Y & N & cm to m & 20 m & Moderate & 308 MHz \\
		RSSI Fingerprinting & RF & Y & Y & cm to m & 30 m & Low & 2.4 GHz \\
		RSSI Positioning & RF & Y & Y & sub-m & & Low & 8 MHz \\
		GPS Sensor Fusion & RF & (Y) & Y & m & Global & Low & 1 Hz \\
		Active Bat & Ultrasound & Y & N & cm & 1000 m^2 & Moderate & 75 Hz \\
		Cricket & Ultrasound & Y & N & cm & 10 m & Low & 1 Hz \\
		Firefly & IR & Y & N & mm & m & High &  \\
		IR Smart Devices & IR & Y & N & cm & 100m^2 & Low &  \\
		\bottomrule
	\end{tabularx}
	\caption[Summary of systems 1]{The systems and papers described in \autoref{sec:pos-systems}. Systems and papers are analyzed in regards to the dimensions defined in the section.}
	\label{tab:possys-summary}
\end{sidewaystable}

\newpage
\section{Positioning Systems in Games}
\label{sec:pos-games}

For related work in the context of games, we focus on the following dimensions:

\textbf{Technology:} What type of technology is used for positioning.

\textbf{Performance:} How well does the system perform in regards to accuracy, coverage, etc.

Games might feature connectivity between tracked devices. Which is highly important for our design

\textbf{Connectivity:} Connectivity between tracked devices is a main concern in our project.

Another interesting subject is how inaccuracies were coped with and what methods were used to reduce inaccuracy.

\textbf{Methods:} How users/designers dealt with inaccuracy problems.

Most papers referred to utilize some sort of GPS implementation with modifications to improve the performance. 

Flintham et all's work \citep{Flintham2003:WOMOEWMMRG} describes games created as a collaboration between online and in-the-field users from 2001, in the early days of mobile devices. “Can You See Me Now” is about mobile users (chasers) in the game field of London City catching online users (runners) moving avatars on the same map through virtual movement input. Employing only GPS and WLAN and internet for connectivity, the median error rate of positioning was approximately 10 m. To deal with inaccurate positioning, they utilized sound feedback. Users in the field could communicate verbally and often described their situation when GPS signal was unreliable or lost. This would in turn allow online users to identify possible nearby locations of the chasers in the field. Although this is not an increase in accuracy directly, it helps users circumvent some of the inaccuracy rates the system had.

Although not a game, \citep{Macleod2009:Tvoangpsfapmpifh} describes a study of hockey players’ movement throughout plans of circuit and movement patterns. Making use of high spec GPS receivers and accelerometer to track movement, they were able to achieve accuracies of sub 0,1 m. Getting actual position was done through gates, where the exact location, time and speed would be stored. 
Note that the positioning aspect is merely a method used to study the movement patterns and not the actual goal of the study.

In Leser et all's work \citep{Leser2011:LPSiGS}. They describe intelligent spaces as candidates for positioning systems. Using an estimated camera position by regarding the footage of that camera plus knowledge of the location, they describe a system able to estimate positions of soccer players and the location of the ball. Thereby allowing analysis of the flow of a soccer game at a tactical level. They describe patterns learnable by machine learning approaches such as neural networks for constructing models able to describe games at levels such as analytical, tactical and situative. These models can then be used to analyze teams and prepare accordingly. As an example, they mention the issue of pressing and different individual styles of pressing can be captured and analyzed by their system. The accuracy of positioning of the system is extremely reliant on the initial estimates of camera position. 
They also mention another intelligent space, where wearable sensors are employed to keep track of physiology of the human body during exercise and sport. Although not related to positioning, it is something easily integrable in our context.

\autoref{tab:locgame-summary} summarizes the important points of the systems for games presented above:

\begin{sidewaystable}[ph!]
    \myfloatalign
  \begin{tabularx}{\textwidth}{Xccccc} \toprule
    \tableheadline{System} & \tableheadline{Technology} & \tableheadline{Connectivity} & \tableheadline{Accuracy} & \tableheadline{Coverage} & \tableheadline{Methods} \\ \midrule
    CYSMN? & GPS & WLAN, Internet & 10 m & Part of London & Audio Feedback \\
    Hockey Study & GPS, Accelerometer & & 0,1 m & 5000 m^2 &  \\
		Soccer IS & Camera, Machine Learning & & 15-20 cm & 6000 m^2 & \\
    \bottomrule
  \end{tabularx}
  \caption[Summary of systems 2]{The systems and papers described in \autoref{sec:pos-games}. Systems and papers are analyzed in regards to the dimensions defined in the section.}
  \label{tab:locgame-summary}
\end{sidewaystable}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ClassicThesis"
%%% ispell-dictionary: "british" ***
%%% mode:flyspell ***
%%% mode:auto-fill ***
%%% fill-column: 78 ***
%%% End:
