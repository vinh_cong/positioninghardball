\chapter{Evaluation}
\label{cha:evaluation}

Having built the equivalent of a experimental setup, it is time to use
the implementation to test the hypotheses.

It is a good idea at this point to remind the reader of the original
hypotheses, so that you can communicate clearly how evaluating these
hypotheses can be broken down into stages and subquestions manifested as a
series of experiments.

One structured approach to performing and reporting on these experiments is
to follow the following pattern for every single experiment:

\begin{enumerate}
\item What is the purpose of the experiment?
\item What is the expected outcome?
\item What are the parameters under which the experiment takes place?
\item What are the results?
\item How do the results align with the expected outcome? If they do not
  align, why is that so?
\end{enumerate}

Results should be presented summarised. For quantitative experiments, this
will usually be in the form of tables and graphs.  Remember to note the
number of times experiments were repeated, as well as averages, and standard
deviations (in percent of the mean).  There is much more to the proper
statistical evaluation of experimental data than can be expounded upon here,
but I turn the reader's attention to \citep{Downey2011:TSPASFP2011}, which
is freely available and written with CS students in mind.

If your results are of a qualitative nature, the summaries will depend on
the type of investigation you have done. It can be carefully annotated
recordings of specific incidents of the system in use; analysis with quotes
from interviews; or results from questionnaires and other investigations.
Whole transcripts belong in an appendix, but excerpts are fine in the main
text.



\begin{figure}
  \myfloatalign
  \begin{tikzpicture}
    \begin{axis} [xlabel=time (s), ylabel=value]
      \addplot[
      sharp plot,
      error bars,
      y dir=both,
      y explicit,
      error bar style={red}
      ] table[
      x=x,
      y=avgy,
      y error=stddevp]{./data/example.csv};
    \end{axis}
  \end{tikzpicture}
  \caption[A graph with error bars]{Generating figures directly in \LaTeX\ is possible, and they can be very consistent stylewise with the rest of the document.}
  \label{fig:pretty-graph}
\end{figure}



\begin{table}
  \myfloatalign
  \pgfplotstabletypeset[
  every head row/.style={%Define the lines and alternating row backgrounds
    before row=\toprule,
    after row=\midrule
  },
  every even row/.style={
    before row={\rowcolor[gray]{0.95}}},
  every last row/.style={
    after row=\bottomrule
  },
  columns={x, avgy, stddevpp},%Style the chosen columns and specify their alignment etc
  columns/x/.style={
    column name=\textsc{time},
    dec sep align,
    fixed,
    fixed zerofill,
    precision=2
  },
  columns/avgy/.style={
    column name=\textsc{value},
    dec sep align,
    fixed,
    fixed zerofill,
    precision=2
  },
  columns/stddevpp/.style={
    column name=$\sigma\%$,
    dec sep align,
    fixed,
    fixed zerofill,
    precision=1
  },
  ]{./data/example.csv}
  % \tableheadline{labitur bonorum pri no} & \tableheadline{que vista}
  % & \tableheadline{human} \\ \midrule
  \caption[An auto-generated table]{This table has been generated from a
    .csv file, which sometimes can be very handy and a great timesaver. Note, how numbers have been normalised and aligned properly.}
  \label{tab:pretty-table}
\end{table}



\section{Some Notes on Tables, Graphs, and Figures}
\label{sec:some-notes-tables}

It is paramount that you clearly and concisely present your results, and
that usually entails documentation in the form of tables or figures.  Such
things are more informative if they are clearly formatted and legible.

There are many tools with which you can plot graphs of your data. I would
suggest that you use something that can be scripted, so you can tweak your
parameters, run the evaluation, and see the fresh results, again and
again. Having to, \eg copy and paste or import data into an application will
slow you down, and might well lead to confusion---is this the new data or
not? A small investment upfront in scripting everything will lead to
savings in both time and frustration later on.

If you are fluent in Python,
Matplotlib\footnote{\url{https://matplotlib.org/} accessed 2018/6/27.} is very good, and can be
used interactively in
Jupyter\footnote{\url{https://jupyter.org/} accessed 2018/6/27.}. Gnuplot\footnote{\url{http://gnuplot.info/} accessed 2018/6/27.}
is another powerful choice, and probably easier to get started with. If you
know R\footnote{\url{https://www.r-project.org/} accessed 2018/6/27.},
ggplot2\footnote{\url{http://ggplot2.org/} accessed 2018/6/27.} is the obvious route.  For
web-based systems, D3.js\footnote{\url{https://d3js.org/} accessed 2018/6/27.} is quite popular.

As long as your tool of choice can generate PDFs, you should be golden, as
those are easy to include in PDF\LaTeX\ and scale well. It is a very good
idea to try to print pages with figures on them--you may \emph{think} that
legends and labels are legible when looking at a zoomed-in PDF, but a paper
copy might well tell a different story.

If you output your data to comma or tab separated files, you can even
generate your plots and tables directly in \LaTeX\ using
\textsc{Pgfplots}\footnote{\url{http://pgfplots.sourceforge.net/} accessed
  2018/6/27} (Share\LaTeX\ has a gentle
introduction\footnote{\url{https://www.sharelatex.com/learn/Pgfplots_package}
  accessed 2018/6/17}): \autoref{fig:pretty-graph} was generated using
\textsc{Pgfplots} and \autoref{tab:pretty-table} using
\textsc{PgfplotsTable}\footnote{\url{http://pgfplots.sourceforge.net/pgfplotstable.pdf}
  accessed 2018/6/27.}.  Doing this can quickly save you time, as especially
\LaTeX\ tables are tedious and error prone to edit. For more help on tables,
see the \LaTeX\ Wiki
books\footnote{\url{https://en.wikibooks.org/wiki/LaTeX/Tables} accessed
  2018/6/27.}.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ClassicThesis"
%%% ispell-dictionary: "british" ***
%%% mode:flyspell ***
%%% mode:auto-fill ***
%%% fill-column: 76 ***
%%% End:
