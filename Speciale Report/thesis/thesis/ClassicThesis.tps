[FormatInfo]
Type=TeXnicCenterProjectSessionInformation
Version=2

[Frame0]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=LaTeXView
Document=ClassicThesis.tex

[Frame0_View0,0]
TopLine=76
Cursor=4342

[Frame1]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=LaTeXView
Document=Chapters\Chapter01-Introduction.tex

[Frame1_View0,0]
TopLine=0
Cursor=0

[Frame2]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=LaTeXView
Document=Chapters\Chapter02-Related-Work.tex

[Frame2_View0,0]
TopLine=165
Cursor=9796

[Frame3]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=LaTeXView
Document=Chapters\Chapter03-Analysis.tex

[Frame3_View0,0]
TopLine=16
Cursor=1392

[Frame4]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=LaTeXView
Document=Chapters\Chapter04-Design-and-Methodology.tex

[Frame4_View0,0]
TopLine=166
Cursor=9547

[Frame5]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=LaTeXView
Document=Chapters\Chapter05-Implementation.tex

[Frame5_View0,0]
TopLine=0
Cursor=1132

[Frame6]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=LaTeXView
Document=classicthesis-config.tex

[Frame6_View0,0]
TopLine=33
Cursor=3126

[Frame7]
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-1
MaxPos.y=-1
NormalPos.left=4
NormalPos.top=26
NormalPos.right=1562
NormalPos.bottom=539
Class=BibTeXView
Document=Bibliography.bib

[Frame7_View0,0]
TopLine=155
Cursor=7331

[SessionInfo]
FrameCount=8
ActiveFrame=5

